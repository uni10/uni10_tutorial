pyuni10.BD_OUT
==============

.. py:data:: pyuni10.BD_OUT = -1

   The value defines an **out-going** bond


