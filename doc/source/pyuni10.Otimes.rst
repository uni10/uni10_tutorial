pyuni10.otimes
==============

.. py:currentmodule:: pyuni10

.. py:function:: Otimes(Ta, Tb) 

   Tensor product of two matrices/tensors.

   :param Ta: *Matrix* / *UniTensor*
   :param Tb: *Matrix* / *UniTensor*
   :type Ta: Matrix / UniTensor
   :type Tb: Matrix / UniTensor
   :return: tensor product of Ta and Tb
   :rtype: Matrix / UniTensor
