pyuni10.PRT_ODD
===============

.. py:data:: pyuni10.PRT_ODD = 1

   Value defines particle parity **odd** in a bosnic system
