pyuni10.PRTF_ODD
================

.. py:data:: pyuni10.PRTF_ODD = 1

   Value defines particle parity **odd** in a fermionic system
