.. Uni10-Tutorials documentation master file, created by
   sphinx-quickstart on Sun Nov 23 17:19:28 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Uni10-Tutorials's documentation!
===========================================

Here you can find 

* **pyuni10**: Python wrappers for Uni10.
* **Lectures**: iPython notebooks for lectures.
* Reference Manual for pyuni10.

This tutorial is released under a CC BY-NC-SA 3.0 license. The Uni10
code is released under a LGPL license.

`Uni10 homepage <http://uni10.org/>`_ 

.. toctree::
   :maxdepth: 2

   setup
   lecture1
   lecture2
   lecture3
   pyuni10


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

