=========================
Tutorial 2
=========================




In this lecture, we will use pyuni10 to implement the iTEBD algorithm
with **U(1)** symmety, and use it to study the Heisenberg model. 

Symmetric Tensors
=================
We will extend our discussion on pyuni10 to construct tensors with **U(1)** symmetry. 

A symmetric tensor can be regarded as a tensor with bonds carrying quantum numbers. 

.. image:: Figures/U1_tensor.jpg
   :width: 300pt

For U(1) invariant tensors, the total U(1) quantum number of the tensor is zero. That is, 
the U(1) quantum numbers corresponding to incoming and outgoing indices,

.. math::
  
   N_{\rm in}=\sum_{n_l \in I} n_l,\quad N_{\rm out}=\sum_{n_l \in O} n_l,

should be equal.

  One can form a U(1) invariant tensor network made of U(1) invariant tensors

.. image:: Figures/U1_Network.jpg
   :width: 300pt 

In pyuni10, a U(1) symmetric tensor is defined as a UniTensor object, which consists of bonds 
carrying quantum numbers.  

.. image:: Figures/UniTensor.jpg
   :width: 250pt 


You can download the ipython notebook for this part of the tutorial: :download:`Tutorial2-1.ipynb<../../python/lecture2/Tutorial2-1.ipynb>`.

iTEBD with U(1) Symmetry
=========================

We will use iTEBD with U(1) symmetry to study the 1D Heisenberg model,

.. math::
   
   H=\sum_{\langle ij\rangle} \mathbf{S}_i\cdot \mathbf{S}_j 


You can download the ipython notebook for this part of the tutorial: :download:`Tutorial2-2.ipynb<../../python/lecture2/Tutorial2-2.ipynb>`.


.. X warning:: Under Construction!!

References
==========

Tensor Network with U(1) Symmetry
---------------------------------

 1. Sukhwinder Singh, Robert N. C. Pfeifer, and Guifre Vidal,  Tensor network states and algorithms 
 in the presence of a global U(1) symmetry, `Phys. Rev. B 83 115125 (2011) <http://journals.aps.org/prb/abstract/10.1103/PhysRevB.83.115125>`_,  http://arxiv.org/abs/1008.4774
