pyuni10.PRTF_EVEN
=================

.. py:data:: pyuni10.PRTF_EVEN = 0

   Value defines particle parity **even** in a fermionic system
