pyuni10.contract
================

.. py:currentmodule:: pyuni10

.. py:function:: contract(Ta, Tb, fast=False)

   Contract out the bonds with common labels in *UniTensors*  Ta and Tb. 
   Ta and Tb are not copied and are permuted in-place, thus the process uses less memory than (Ta * Tb). 
   
  :param UniTensor Ta: a  *UniTensor*
  :param UniTensor Tb: a  *UniTensor*
  :param bool fast: if fast set to True, the tensors Ta and Tb will not be permuted back to the origin labels after contraction.
  :return:  a new *UniTensor* with remaining bonds
  :rtype:  UniTensor
