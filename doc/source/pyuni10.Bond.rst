pyuni10.Bond
============

.. py:currentmodule:: pyuni10

.. py:class:: Bond

   Proxy of C++ uni10::Bond class

   Class for bonds


.. py:method:: Bond(bdtype,  dim )

.. py:method:: Bond(bdtype,  qnums )

.. py:method:: Bond(bd)

   Creates a Bond object

   :param BondType bdtype: BD_IN / BD_OUT
   :param int dim: dimension of the bond (no symmetry)
   :param qnums: list of quantum numbers
   :type qnums: array of Qnums
   :param Bond bd: a Bond object
   :return: a Bond object
   :rtype: Bond


   .. rubric:: Methods

   .. py:method:: Bond.Qlist()

      Returns a tuple of quantum numbers associate with *Bond*

      :return: a tuple of quantum numbers
      :rtype: tuple of Qnum

   .. py:method:: Bond.assign(bdtype, dim)

   .. py:method:: Bond.assign(bdtype, qnums)

      Set the dimensions / quantum numbers of a bond.

      :param BondType bdtype: BD_IN / BD_OUT
      :param int dim: dimension of a bond (no symmetry)
      :param qnum: arrays of quantum numbers
      :type qnum: arrays of Qnum

   .. py:method:: Bond.change(bdtype)

      Changes the type of *Bond*, and changes the Qnums of *Bond* . If the bond is changed from in-coming(BD_IN) to out-going(BD_OUT) type or vice versa,
      the resulting Qnums is -Qnums.

      :param BondType bdtype: BD_IN / BD_OUT
      :return: _Bond_ with type bdtype
      :rtype: Bond

   .. py:method:: Bond.combine(bd)

      Merge bd into *Bond*.

      :param Bond bd: bond to be merged
      :return: merged bond
      :rtype: Bond

   .. py:method:: Bond.degeneracy()

      Returns a dictionary of quantum numbers as the key and the corresponding number of degeneracy as the value ({Qnum: int}).

      :return: dictionary of quantum number:degeneracy pairs
      :rtype: dict ({Qnum: int})

   .. py:method:: Bond.dim()

      Returns the total dimension of *Bond*

      :return: dimension of *Bond*
      :rtype: int

   .. py:method:: Bond.type()

      Returns the type of  *Bond*

      :return: BD_IN / BD_OUT
      :rtype: BondType
