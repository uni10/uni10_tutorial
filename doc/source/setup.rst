==================
Setting Up pyuni10
==================

Preparing Your System
---------------------
 You need to have Python 3 installed on your computer.

  * As an integrated distribution, we recommend the `Anaconda <https://www.anaconda.com/distribution/>`_.



Obtaining pyuni10
-----------------

pyuni10 is distributed on `PyPI <https://pypi.python.org/pypi/pyuni10>`_ and can be installed with pip:

.. code-block:: bash

   pip install pyuni10


Runnning Tests
-----------------


Manually run the Python codes in the sequence listed above, or use the :download:`TestUni10.ipynb <../../python/test/TestUni10.ipynb>`.

If you don't see any error messages, **congratulations**, you are ready to go!!!
