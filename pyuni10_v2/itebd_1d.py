import pyuni10 as uni10
import numpy as np
import scipy.linalg
import os
#print("pyUni10 {}, numpy {}, scipy {}.".format(uni10.__version__, np.__version__, scipy.__version__))
#print("pyUni1i Tutorial: iTEBD-1D")

# physical bonds
d = 2
bdi_p = uni10.Bond(uni10.BD_IN, 2)
bdo_p = uni10.Bond(uni10.BD_OUT, 2)

# single site operators
Id = uni10.UniTensorR([bdi_p, bdo_p], 'Id')
Id.PutBlock(np.array([[1, 0],
                      [0, 1]]))
Sx = uni10.UniTensorR([bdi_p, bdo_p], 'Sx')
Sx.PutBlock(np.array([[0, 1],
                      [1, 0]]))
Sz = uni10.UniTensorR([bdi_p, bdo_p], 'Sz')
Sz.PutBlock(np.array([[+1, 0],
                      [0, -1]]))

# two sites Hamiltonian
def transverseIsing(hx):
    H = uni10.Otimes(Sz,Sz) + 0.5*hx*(uni10.Otimes(Id,Sx)+uni10.Otimes(Sx,Id))
    H.SetName('Ham')
    return H

def transverseIsing2(hx):
    H = uni10.UniTensorR([bdi_p, bdi_p, bdo_p, bdo_p])
    SzSz_M = np.array([[1, 0, 0, 0],
                    [0, -1, 0, 0],
                    [0, 0, -1, 0],
                    [0, 0, 0, 1]])
    field_M = np.array([[0, 1, 1, 0],
                        [1, 0, 0, 1],
                        [1, 0, 0, 1],
                        [0, 1, 1, 0]])

    H.PutBlock(SzSz_M + 0.5*hx*field_M)
    H.SetName('Ham')
    return H

def construct_U(H, dt):
    U = uni10.UniTensorR(H.bond())
    U_M = scipy.linalg.expm(-dt*H.GetBlock())
    U.PutBlock(U_M)
    return U

def init_Gamma(D):
    """Initialize list of Gamma tensors"""
    bdi = uni10.Bond( uni10.BD_IN, D)
    bdo = uni10.Bond( uni10.BD_OUT, D)

    # Gamma tensors: Gammas=[Gamma_a, Gamma_b]
    Gamma_a = uni10.UniTensorR([bdi, bdi_p, bdo], 'Gamma_a')
    Gamma_a.Randomize()
    Gamma_b = uni10.UniTensorR([bdi, bdi_p, bdo], 'Gamma_b')
    Gamma_b.Randomize()
    Gammas = [Gamma_a, Gamma_b]

    return Gammas

def init_Lambda(D):
    """Initialize list of Lambda (diagonal) tensors"""
    bdi = uni10.Bond( uni10.BD_IN, D)
    bdo = uni10.Bond( uni10.BD_OUT, D)

    # Lambda tensors (diagonal): Lambdas=[Lambda_a, Lambda_b]
    Lambda_a = uni10.UniTensorR([bdi, bdo], 'Lambda_a')
    Lambda_a.PutBlock(np.diag(np.sort(np.random.rand(D))))
    Lambda_b = uni10.UniTensorR([bdi, bdo], 'Lambda_b')
    Lambda_b.PutBlock(np.diag(np.sort(np.random.rand(D))))
    Lambdas = [Lambda_a, Lambda_b]

    return Lambdas

def string2net(s):
    """Convert a multi line string into a uni10 network"""
    with open('tmp.net','w') as f:
        f.write(s)
    f.close()
    net = uni10.Network("tmp.net")
    os.remove('tmp.net')
    return net

def construct_Theta_net():
    """Construct Theta network"""
    Theta_net = \
"""LamL: -1; 1
GamL: 1 101; 2
LamC: 2; 3
GamR: 3 102; 4
LamR: 4; -2
U: -101 -102; 101 102
TOUT: -1 -101; -102 -2"""
    return string2net(Theta_net)

def construct_LGLGL_net():
    """Construct Lamba-Gamma-Lambda-Gamma-Lambda network"""
    LGLGL_net = \
"""LamL: -1; 1
GamL: 1 -101; 2
LamC: 2; 3
GamR: 3 -102; 4
LamR: 4; -2
TOUT: -1 -101; -102 -2"""
    return string2net(LGLGL_net)

def construct_Theta(Theta_net, Lambda_l, Gamma_l, Lambda_c, Gamma_r, Lambda_r, U):
    Theta_net.PutTensor("LamL", Lambda_l)
    Theta_net.PutTensor("GamL", Gamma_l)
    Theta_net.PutTensor("LamC", Lambda_c)
    Theta_net.PutTensor("GamR", Gamma_r)
    Theta_net.PutTensor("LamR", Lambda_r)
    Theta_net.PutTensor("U", U)

    Tout = uni10.UniTensorR([])
    Theta_net.Launch(Tout)
    return Tout

def construct_LGLGL(LGLGL_net, Lambda_l, Gamma_l, Lambda_c, Gamma_r, Lambda_r):
    LGLGL_net.PutTensor("LamL", Lambda_l)
    LGLGL_net.PutTensor("GamL", Gamma_l)
    LGLGL_net.PutTensor("LamC", Lambda_c)
    LGLGL_net.PutTensor("GamR", Gamma_r)
    LGLGL_net.PutTensor("LamR", Lambda_r)

    Tout = uni10.UniTensorR([])
    LGLGL_net.Launch(Tout)
    return Tout

def diagonal_tensor_inv(T):
    """Return the inverse tensor of a diagonal tensor"""
    T_M = T.GetBlock()
    T_inv_M = np.diag(1/np.diag(T_M))
    T_inv = uni10.UniTensorR(T.bond())
    T_inv.PutBlock(T_inv_M)
    return T_inv

def half_update(Theta_net, Lambda_l, Gamma_l, Lambda_c, Gamma_r, Lambda_r, U, D_max):
    """Perform half of the 2nd order Suzuki-Trotter update"""
    Theta = construct_Theta(Theta_net, Lambda_l, Gamma_l, Lambda_c, Gamma_r, Lambda_r, U)

    # SVD on Theta
    Theta_M = Theta.GetBlock()
    X, Lambda_V, Y = np.linalg.svd(Theta_M)

    # new bond dimension and bonds
    D = min(Lambda_V.size, D_max)
    bdi_D = uni10.Bond(uni10.BD_IN, D)
    bdo_D = uni10.Bond(uni10.BD_OUT, D)

    # truncate and renormalize Lambda_V; update Lambda_c (diagonal)
    Lambda_V = Lambda_V[:D]
    Lambda_V = Lambda_V / scipy.linalg.norm(Lambda_V)
    Lambda_c_new = uni10.UniTensorR([bdi_D,bdo_D])
    Lambda_c_new.PutBlock(np.diag(Lambda_V))

    # truncate X and Y
    X = X[:,:D]
    Y = Y[:D,:]

    # update Lambda-Gamma_l and Gamma-Lambda_r
    LG_l_new = uni10.UniTensorR([Theta.bond(0), Theta.bond(1), bdo_D])
    LG_l_new.PutBlock(X)
    GL_r_new = uni10.UniTensorR([bdi_D, Theta.bond(2), Theta.bond(3)])
    GL_r_new.PutBlock(Y)
    # GL_r_new = uni10.Permute(GL_r_new, 2) # need more comment here
    uni10.Permute(GL_r_new, 2) # need more comment here, in-place permute now

    # inverse of the Lambda_l and update Gamma_l
    Lambda_l_inv = diagonal_tensor_inv(Lambda_l)
    Lambda_l_inv.SetLabel([-1,1])
    LG_l_new.SetLabel([1,-101,-2])
    Gamma_l_new = uni10.Contract(Lambda_l_inv, LG_l_new)
    # Gamma_l_new = uni10.Permute(Gamma_l_new, 2)
    uni10.Permute(Gamma_l_new, 2)

    # inverse of the Lambda_r and update Gamma_r
    Lambda_r_inv = diagonal_tensor_inv(Lambda_r)
    GL_r_new.SetLabel([-1,-101,1])
    Lambda_r_inv.SetLabel([1,-2])
    Gamma_r_new = uni10.Contract(GL_r_new, Lambda_r_inv)
    # Gamma_r_new = uni10.Permute(Gamma_r_new, 2)
    uni10.Permute(Gamma_r_new, 2)

    return Gamma_l_new, Lambda_c_new, Gamma_r_new

def SuzukiTrotter(Theta_net, Gammas, Lambdas, U, D_max):
    """Perform 2nd order Suzuki-Trotter update"""
    Gammas[0], Lambdas[1], Gammas[1] = half_update(Theta_net, Lambdas[0], Gammas[0], Lambdas[1], Gammas[1], Lambdas[0], U, D_max)
    Gammas[1], Lambdas[0], Gammas[0] = half_update(Theta_net, Lambdas[1], Gammas[1], Lambdas[0], Gammas[0], Lambdas[1], U, D_max)

def Ham_exp(Theta_net, LGLGL_net, Gammas, Lambdas, H):
    Ket = construct_Theta(Theta_net, Lambdas[0], Gammas[0], Lambdas[1], Gammas[1], Lambdas[0], H)
    Bra = construct_LGLGL(LGLGL_net, Lambdas[0], Gammas[0], Lambdas[1], Gammas[1], Lambdas[0])
    T = uni10.Contract(Bra, Ket)
    return T.GetElem()[0]

def Energy(Theta_net, LGLGL_net, Gammas, Lambdas, H):
    H_exp = Ham_exp(Theta_net, LGLGL_net, Gammas, Lambdas, H)
    LGLGL = construct_LGLGL(LGLGL_net, Lambdas[0], Gammas[0], Lambdas[1], Gammas[1], Lambdas[0])
    H_exp /= (uni10.Contract(LGLGL, LGLGL).GetElem()[0]) ** 2
    return H_exp

if __name__ == "__main__":

    D_max = 4
    dt = 1
    steps= 200

    Theta_net = construct_Theta_net()
    LGLGL_net = construct_LGLGL_net()
    H = transverseIsing(1.5)
    U = construct_U(H, dt)

    D_ini = 3
    Gammas = init_Gamma(D_ini)
    Lambdas = init_Lambda(D_ini)

    print('#{:<4}, {:10}'.format('Step', 'E'))
    for s in range(steps):
        SuzukiTrotter(Theta_net, Gammas, Lambdas, U, D_max)
        E = Energy(Theta_net, LGLGL_net, Gammas, Lambdas, H)
        print('{:>4}, {:+10}'.format(s, E))
