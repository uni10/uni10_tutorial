import pyuni10 as uni10
import copy
import numpy as np
import scipy.linalg
import scipy.integrate
import matplotlib.pyplot as plt

#print("pyUni10 {}, numpy {}, scipy {}.".format(uni10.__version__, np.__version__, scipy.__version__))
#print("pyUni10 Tutorial: iTEBD-1D")

# physical bonds
d = 2
bdi_p = uni10.Bond(uni10.BD_IN, 2)
bdo_p = uni10.Bond(uni10.BD_OUT, 2)

# single site operators
Id = uni10.UniTensorR([bdi_p, bdo_p], 'Id')
Id.PutBlock(np.array([[1, 0],
                      [0, 1]]))
Sx = uni10.UniTensorR([bdi_p, bdo_p], 'Sx')
Sx.PutBlock(np.array([[0, 1],
                      [1, 0]]))
Sz = uni10.UniTensorR([bdi_p, bdo_p], 'Sz')
Sz.PutBlock(np.array([[+1, 0],
                      [0, -1]]))

# two sites Hamiltonian
def transverseIsing(hx):
    """Construct the Hamiltonian by the direct product of single site operators"""
    H = uni10.Otimes(Sz,Sz) + 0.5*hx*(uni10.Otimes(Id,Sx)+uni10.Otimes(Sx,Id))
    H.SetName('Ham')
    return H

def transverseIsing2(hx):
    """Construct the Hamiltonian by the block of the two-site operator matrix"""
    H = uni10.UniTensorR([bdi_p, bdi_p, bdo_p, bdo_p])
    SzSz_M = np.array([[1, 0, 0, 0],
                    [0, -1, 0, 0],
                    [0, 0, -1, 0],
                    [0, 0, 0, 1]])
    field_M = np.array([[0, 1, 1, 0],
                        [1, 0, 0, 1],
                        [1, 0, 0, 1],
                        [0, 1, 1, 0]])

    H.PutBlock(SzSz_M + 0.5*hx*field_M)
    H.SetName('Ham')
    return H

def construct_U(H, dt):
    "construct exp(-dt*H)"
    U = uni10.UniTensorR(H.bond())
    U_M = scipy.linalg.expm(-dt*H.GetBlock())
    U.PutBlock(U_M)
    return U

def init_Gamma(D):
    """Initialize the list of the Gamma tensors"""
    bdi = uni10.Bond( uni10.BD_IN, D)
    bdo = uni10.Bond( uni10.BD_OUT, D)

    # Gamma tensors: Gammas=[Gamma_a, Gamma_b]
    Gamma_a = uni10.UniTensorR([bdi, bdi_p, bdo], 'Gamma_a')
    Gamma_a.Randomize()
    Gamma_b = uni10.UniTensorR([bdi, bdi_p, bdo], 'Gamma_b')
    Gamma_b.Randomize()
    Gammas = [Gamma_a, Gamma_b]

    return Gammas

def init_Lambda(D):
    """Initialize the list of the Lambda (diagonal) tensors"""
    bdi = uni10.Bond( uni10.BD_IN, D)
    bdo = uni10.Bond( uni10.BD_OUT, D)

    # Lambda tensors (diagonal): Lambdas=[Lambda_a, Lambda_b]
    Lambda_a = uni10.UniTensorR([bdi, bdo], 'Lambda_a')
    Lambda_a.PutBlock(np.diag(np.sort(np.random.rand(D))))
    Lambda_b = uni10.UniTensorR([bdi, bdo], 'Lambda_b')
    Lambda_b.PutBlock(np.diag(np.sort(np.random.rand(D))))
    Lambdas = [Lambda_a, Lambda_b]

    return Lambdas

def construct_Theta(Theta_net, Lambda_l, Gamma_l, Lambda_c, Gamma_r, Lambda_r, U):
    """Construct Theta tensor"""
    Theta_net.PutTensor("LamL", Lambda_l)
    Theta_net.PutTensor("GamL", Gamma_l)
    Theta_net.PutTensor("LamC", Lambda_c)
    Theta_net.PutTensor("GamR", Gamma_r)
    Theta_net.PutTensor("LamR", Lambda_r)
    Theta_net.PutTensor("U", U)

    Tout = uni10.UniTensorR([])
    Theta_net.Launch(Tout)
    return Tout

def construct_LGLGL(LGLGL_net, Lambda_l, Gamma_l, Lambda_c, Gamma_r, Lambda_r):
    LGLGL_net.PutTensor("LamL", Lambda_l)
    LGLGL_net.PutTensor("GamL", Gamma_l)
    LGLGL_net.PutTensor("LamC", Lambda_c)
    LGLGL_net.PutTensor("GamR", Gamma_r)
    LGLGL_net.PutTensor("LamR", Lambda_r)

    Tout = uni10.UniTensorR([])
    LGLGL_net.Launch(Tout)
    return Tout

def diagonal_tensor_inv(T):
    """Return the inverse tensor of a diagonal tensor"""
    T_M = T.GetBlock()
    T_inv_M = np.diag(1/np.diag(T_M))
    T_inv = uni10.UniTensorR(T.bond())
    T_inv.PutBlock(T_inv_M)
    return T_inv

def half_update(Theta_net, Lambda_l, Gamma_l, Lambda_c, Gamma_r, Lambda_r, U, D_max):
    """Perform half of the 2nd order Suzuki-Trotter update"""
    Theta = construct_Theta(Theta_net, Lambda_l, Gamma_l, Lambda_c, Gamma_r, Lambda_r, U)

    # SVD on Theta
    Theta_M = Theta.GetBlock()
    X, Lambda_V, Y = np.linalg.svd(Theta_M)

    # new bond dimension and bonds
    D = min(Lambda_V.size, D_max)
    bdi_D = uni10.Bond(uni10.BD_IN, D)
    bdo_D = uni10.Bond(uni10.BD_OUT, D)

    # truncate and renormalize Lambda_V; update Lambda_c (diagonal)
    Lambda_V = Lambda_V[:D]
    Lambda_V = Lambda_V / scipy.linalg.norm(Lambda_V)
    Lambda_c_new = uni10.UniTensorR([bdi_D,bdo_D])
    Lambda_c_new.PutBlock(np.diag(Lambda_V))

    # truncate X and Y
    X = X[:,:D]
    Y = Y[:D,:]

    # update Lambda-Gamma_l and Gamma-Lambda_r
    LG_l_new = uni10.UniTensorR([Theta.bond(0), Theta.bond(1), bdo_D])
    LG_l_new.PutBlock(X)
    GL_r_new = uni10.UniTensorR([bdi_D, Theta.bond(2), Theta.bond(3)])
    GL_r_new.PutBlock(Y)
    # GL_r_new = uni10.Permute(GL_r_new, 2) # need more comment here
    uni10.Permute(GL_r_new, 2) # need more comment here

    # inverse of the Lambda_l and update Gamma_l
    Lambda_l_inv = diagonal_tensor_inv(Lambda_l)
    Lambda_l_inv.SetLabel([-1,1])
    LG_l_new.SetLabel([1,-101,-2])
    Gamma_l_new = uni10.Contract(Lambda_l_inv, LG_l_new)
    # Gamma_l_new = uni10.Permute(Gamma_l_new, 2)
    uni10.Permute(Gamma_l_new, 2)

    # inverse of the Lambda_r and update Gamma_r
    Lambda_r_inv = diagonal_tensor_inv(Lambda_r)
    GL_r_new.SetLabel([-1,-101,1])
    Lambda_r_inv.SetLabel([1,-2])
    Gamma_r_new = uni10.Contract(GL_r_new, Lambda_r_inv)
    # Gamma_r_new = uni10.Permute(Gamma_r_new, 2)
    uni10.Permute(Gamma_r_new, 2)

    return Gamma_l_new, Lambda_c_new, Gamma_r_new

def SuzukiTrotter(Theta_net, Gammas, Lambdas, U, D_max):
    """Perform 2nd order Suzuki-Trotter update"""
    Gammas[0], Lambdas[1], Gammas[1] = half_update(Theta_net, Lambdas[0], Gammas[0], Lambdas[1], Gammas[1], Lambdas[0], U, D_max)
    Gammas[1], Lambdas[0], Gammas[0] = half_update(Theta_net, Lambdas[1], Gammas[1], Lambdas[0], Gammas[0], Lambdas[1], U, D_max)

def Ham_exp(Theta_net, LGLGL_net, Gammas, Lambdas, H):
    Ket = construct_Theta(Theta_net, Lambdas[0], Gammas[0], Lambdas[1], Gammas[1], Lambdas[0], H)
    Bra = construct_LGLGL(LGLGL_net, Lambdas[0], Gammas[0], Lambdas[1], Gammas[1], Lambdas[0])
    # print(uni10.Dagger(Bra))
    # print(Ket)
    # exit()
    T = uni10.Contract(Bra, Ket)
    return T.GetElem()[0]

def Energy(Theta_net, LGLGL_net, Gammas, Lambdas, H):
    H_exp = Ham_exp(Theta_net, LGLGL_net, Gammas, Lambdas, H)
    LGLGL = construct_LGLGL(LGLGL_net, Lambdas[0], Gammas[0], Lambdas[1], Gammas[1], Lambdas[0])
    H_exp /= (uni10.Contract(LGLGL, LGLGL).GetElem()[0]) ** 2
    return H_exp


def exp_val( net, gamA, gamB, lamA, lamB, op ):
    """
    Return expectation value of local operator
    """
    tens = [lamB, gamA, lamA, gamB, lamB, op, lamB, uni10.Dagger(gamA), lamA, uni10.Dagger(gamB), lamB]
    for i in range( len(tens) ):
        net.PutTensor(i, tens[i])

    Tout = uni10.UniTensorR([])
    net.Launch(Tout)
    return net.Launch()[0]

def E0_exact(h):
    f = lambda x,h : -2*np.sqrt(1+h**2-2*h*np.cos(x))/np.pi/2.
    E0 = scipy.integrate.quad(f, 0, np.pi, args=(h,))[0]
    return E0

def run_single(D_ini, D_max, h, dt_step):

    H = transverseIsing(h)
    E0 = E0_exact(h)

    Gammas = init_Gamma(D_ini)
    Lambdas = init_Lambda(D_ini)

    t = 0.0
    s_rc = []
    t_rc = []
    e_rc = []
    for (dt, steps) in dt_step:
        for s in range(steps):
            U = construct_U(H, dt)
            SuzukiTrotter(Theta_net, Gammas, Lambdas, U, D_max)
            E = Energy(Theta_net, LGLGL_net, Gammas, Lambdas, H)
            # Sz_exp = exp_val( expv1_net, Gammas[0], Gammas[1], Lambdas[0], Lambdas[1], Sz )
            print('{:>4}, {:.4}, {:+10}'.format(s, t, E-E0))
            s_rc.append(s)
            t_rc.append(t)
            e_rc.append(E)
            t = t + dt
    return s_rc, t_rc, e_rc

# __main__
if __name__ == "__main__":

    Theta_net = uni10.Network("Theta.net")
    LGLGL_net = uni10.Network("LGLGL.net")
    expv1_net = uni10.Network("expv1.net")

    h = 0.5
    E0 = E0_exact(h)

    plt.figure()

    dt_stsp = [(0.1, 101), (0.01, 201), (0.001, 201)]
    # dt_stsp = [(0.001, 1001)]

    # s, t, e = run_single(1, 2, h, dt_stsp)
    # plt.plot(e-E0*np.ones_like(e), label="D=10")

    s, t, e = run_single(3, 20, h, dt_stsp)
    plt.plot(e-E0*np.ones_like(e), label="D=20")

    plt.semilogy()
    # plt.ylim([0,0.02])
    plt.grid()
    plt.legend()
    plt.show()
